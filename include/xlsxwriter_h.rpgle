**FREE

/if not defined(LIBXLSXWRITER)
/define LIBXLSXWRITER


//
// Common prototypes by Mihael Schmidt
// 
// Chart prototypes by Jason Olson
//


//
// Templates
//

dcl-ds lxw_workbook_options qualified template;
  constant_memory int(3);
  dummy char(15);
  tmpdir pointer;
end-ds;

dcl-ds lxw_image_options qualified template;
  xOffset int(10);
  yOffset int(10);
  xScale float(8);
  yScale float(8);
end-ds;

dcl-ds lxw_datetime qualified template align(*full);
  year int(10) inz(0);
  month int(10) inz(0);
  day int(10) inz(0);
  hour int(10) inz(0);
  min int(10) inz(0);
  sec float(8) inz(0.0);
end-ds;

dcl-ds lxw_header_footer_options_t qualified template;
  margin float(8);
end-ds;


//
// Constants
//

dcl-c LXW_DEFAULT_ROW_HEIGHT 15,0;
dcl-c LXW_DEFAULT_COLUMN_WIDTH 8,43;

dcl-c LXW_BORDER_NONE 0;
dcl-c LXW_BORDER_THIN 1;
dcl-c LXW_BORDER_MEDIUM 2;
dcl-c LXW_BORDER_DASHED 3;
dcl-c LXW_BORDER_DOTTED 4;
dcl-c LXW_BORDER_THICK 5;
dcl-c LXW_BORDER_DOUBLE 6;
dcl-c LXW_BORDER_HAIR 7;
dcl-c LXW_BORDER_MEDIUM_DASHED 8;
dcl-c LXW_BORDER_DASH_DOT 9;
dcl-c LXW_BORDER_MEDIUM_DASH_DOT 10;
dcl-c LXW_BORDER_DASH_DOT_DOT 11;
dcl-c LXW_BORDER_MEDIUM_DASH_DOT_DOT 12;
dcl-c LXW_BORDER_SLANT_DASH_DOT 13;

dcl-c LXW_PATTERN_NONE 0;
dcl-c LXW_PATTERN_SOLID 1;
dcl-c LXW_PATTERN_MEDIUM_GRAY 2;
dcl-c LXW_PATTERN_DARK_GRAY 3;
dcl-c LXW_PATTERN_LIGHT_GRAY 4;
dcl-c LXW_PATTERN_DARK_HORIZONTAL 5;
dcl-c LXW_PATTERN_DARK_VERTICAL 6;
dcl-c LXW_PATTERN_DARK_DOWN 7;
dcl-c LXW_PATTERN_DARK_UP 8;
dcl-c LXW_PATTERN_DARK_GRID 9;
dcl-c LXW_PATTERN_DARK_TRELLIS 10;
dcl-c LXW_PATTERN_LIGHT_HORIZONTAL 11;
dcl-c LXW_PATTERN_LIGHT_VERTICAL 12;
dcl-c LXW_PATTERN_LIGHT_DOWN 13;
dcl-c LXW_PATTERN_LIGHT_UP 14;
dcl-c LXW_PATTERN_LIGHT_GRID 15;
dcl-c LXW_PATTERN_LIGHT_TRELLIS 16;
dcl-c LXW_PATTERN_GRAY_125 17;
dcl-c LXW_PATTERN_GRAY_625 18;

dcl-c LXW_COLOR_BLACK 0;
dcl-c LXW_COLOR_BLUE 255;
dcl-c LXW_COLOR_BROWN 8388608;
dcl-c LXW_COLOR_CYAN 65535;
dcl-c LXW_COLOR_GRAY 8421504;
dcl-c LXW_COLOR_GREEN 32768;
dcl-c LXW_COLOR_LIME 65280;
dcl-c LXW_COLOR_MAGENTA 16711935;
dcl-c LXW_COLOR_NAVY 128;
dcl-c LXW_COLOR_ORANGE 16737792;
dcl-c LXW_COLOR_PINK 16711935;
dcl-c LXW_COLOR_PURPLE 8388736;
dcl-c LXW_COLOR_RED 16711680;
dcl-c LXW_COLOR_SILVER 12632256;
dcl-c LXW_COLOR_YELLOW 16776960;
dcl-c LXW_COLOR_WHITE 16777215;

dcl-c LXW_ALIGN_NONE 0;
dcl-c LXW_ALIGN_LEFT 1;
dcl-c LXW_ALIGN_CENTER 2;
dcl-c LXW_ALIGN_RIGHT 3;
dcl-c LXW_ALIGN_FILL 4;
dcl-c LXW_ALIGN_JUSTIFY 5;
dcl-c LXW_ALIGN_CENTER_ACROSS 6;
dcl-c LXW_ALIGN_DISTRIBUTED 7;
dcl-c LXW_ALIGN_VERTICAL_TOP 8;
dcl-c LXW_ALIGN_VERTICAL_BOTTOM 9;
dcl-c LXW_ALIGN_VERTICAL_CENTER 10;
dcl-c LXW_ALIGN_VERTICAL_JUSTIFY 11;
dcl-c LXW_ALIGN_VERTICAL_DISTRIBUTED 12;

dcl-c LXW_FORMAT_SUPERSCRIPT 1;
dcl-c LXW_FORMAT_SUBSCRIPT 2;

dcl-c LXW_UNDERLINE_SINGLE 1;
dcl-c LXW_UNDERLINE_DOUBLE 2;
dcl-c LXW_UNDERLINE_SINGLE_ACCOUNTING 3;
dcl-c LXW_UNDERLINE_DOUBLE_ACCOUNTING 4;

dcl-c LXW_CHART_AREA 1;
dcl-c LXW_CHART_AREA_STACKED 2;
dcl-c LXW_CHART_AREA_STACKED_PERCENT 3;
dcl-c LXW_CHART_BAR 4;
dcl-c LXW_CHART_BAR_STACKED 5;
dcl-c LXW_CHART_BAR_STACKED_PERCENT 6;
dcl-c LXW_CHART_COLUMN 7;
dcl-c LXW_CHART_COLUMN_STACKED 8;
dcl-c LXW_CHART_COLUMN_STACKED_PERCENT 9;
dcl-c LXW_CHART_DOUGHNUT 10;
dcl-c LXW_CHART_LINE 11;
dcl-c LXW_CHART_PIE 12;
dcl-c LXW_CHART_SCATTER 13;
dcl-c LXW_CHART_SCATTER_STRAIGHT 14;
dcl-c LXW_CHART_SCATTER_STRAIGHT_WITH_MARKERS 15;
dcl-c LXW_CHART_SCATTER_SMOOTH 16;
dcl-c LXW_CHART_SCATTER_SMOOTH_WITH_MARKERS 17;
dcl-c LXW_CHART_RADAR 18;
dcl-c LXW_CHART_RADAR_WITH_MARKERS 19;
dcl-c LXW_CHART_RADAR_FILLED 20;
 
dcl-c LXW_CHART_LEGEND_RIGHT 1;
dcl-c LXW_CHART_LEGEND_LEFT 2;
dcl-c LXW_CHART_LEGEND_TOP 3;
dcl-c LXW_CHART_LEGEND_BOTTOM 4;
dcl-c LXW_CHART_LEGEND_OVERLAY_RIGHT 5;
dcl-c LXW_CHART_LEGEND_OVERLAY_LEFT 6;
 
dcl-c LXW_CHART_AXIS_LABEL_POSITION_NEXT_TO 0;
dcl-c LXW_CHART_AXIS_LABEL_POSITION_HIGH 1;
dcl-c LXW_CHART_AXIS_LABEL_POSITION_LOW 2;
dcl-c LXW_CHART_AXIS_LABEL_POSITION_NONE 3;
 
dcl-c LXW_CHART_LABEL_POSITION_DEFAULT 0;
dcl-c LXW_CHART_LABEL_POSITION_CENTER 1;
dcl-c LXW_CHART_LABEL_POSITION_RIGHT 2;
dcl-c LXW_CHART_LABEL_POSITION_LEFT 3;
dcl-c LXW_CHART_LABEL_POSITION_ABOVE 4;
dcl-c LXW_CHART_LABEL_POSITION_BELOW 5;
dcl-c LXW_CHART_LABEL_POSITION_INSIDE_BASE 6;
dcl-c LXW_CHART_LABEL_POSITION_INSIDE_END 7;
dcl-c LXW_CHART_LABEL_POSITION_OUTSIDE_END 8;
dcl-c LXW_CHART_LABEL_POSITION_BEST_FIT 9;
 
dcl-c LXW_CHART_LABEL_SEPARATOR_COMMA 0;
dcl-c LXW_CHART_LABEL_SEPARATOR_SEMICOLON 1;
dcl-c LXW_CHART_LABEL_SEPARATOR_PERIOD 2;
dcl-c LXW_CHART_LABEL_SEPARATOR_NEWLINE 3;
dcl-c LXW_CHART_LABEL_SEPARATOR_SPACE 4;
 
dcl-c LXW_CHART_AXIS_TYPE_X 0;
dcl-c LXW_CHART_AXIS_TYPE_Y 1;
 
dcl-c LXW_CHART_AXIS_UNITS_NONE 0;
dcl-c LXW_CHART_AXIS_UNITS_HUNDREDS 1;
dcl-c LXW_CHART_AXIS_UNITS_THOUSANDS 2;
dcl-c LXW_CHART_AXIS_UNITS_TEN_THOUSANDS 3;
dcl-c LXW_CHART_AXIS_UNITS_HUNDRED_THOUSANDS 4;
dcl-c LXW_CHART_AXIS_UNITS_MILLIONS 5;
dcl-c LXW_CHART_AXIS_UNITS_TEN_MILLIONS 6;
dcl-c LXW_CHART_AXIS_UNITS_HUNDRED_MILLIONS 7;
dcl-c LXW_CHART_AXIS_UNITS_BILLIONS 8;
dcl-c LXW_CHART_AXIS_UNITS_TRILLIONS 9;


//
// Prototypes
//
dcl-pr workbook_new pointer extproc('workbook_new');
  name pointer value options(*string);
end-pr;
dcl-pr workbook_new_opt pointer extproc('workbook_new_opt');
  name pointer value options(*string);
  options likeds(lxw_workbook_options);
end-pr;

dcl-pr workbook_add_worksheet pointer extproc('workbook_add_worksheet');
  workbook pointer value;
  name pointer value options(*string);
end-pr;

dcl-pr workbook_close int(10) extproc('workbook_close');
  workbook pointer value;
end-pr;

dcl-pr workbook_add_format pointer extproc('workbook_add_format');
  workbook pointer value;
end-pr;

dcl-pr worksheet_write_string int(10) extproc('worksheet_write_string');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_formula int(10) extproc('worksheet_write_formula');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_boolean int(10) extproc('worksheet_write_boolean');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  value int(10) value;
  format pointer value;
end-pr;

dcl-pr worksheet_write_url int(10) extproc('worksheet_write_url');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  url pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_blank int(10) extproc('worksheet_write_blank');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  format pointer value;
end-pr;

dcl-pr worksheet_insert_image int(10) extproc('worksheet_insert_image');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  filename pointer value options(*string);
end-pr;

dcl-pr worksheet_insert_image_opt int(10) extproc('worksheet_insert_image_opt');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  filename pointer value options(*string);
  imageOptions likeds(lxw_image_options);
end-pr;

dcl-pr worksheet_set_row int(10) extproc('worksheet_set_row');
  worksheet pointer value;
  row int(10) value;
  height float(8) value;
  format pointer value;
end-pr;

dcl-pr worksheet_set_row_opt int(10) extproc('worksheet_set_row_opt');
  worksheet pointer value;
  row int(10) value;
  height float(8) value;
  format pointer value;
  options pointer value;
end-pr;

dcl-pr worksheet_set_column int(10) extproc('worksheet_set_column');
  worksheet pointer value;
  columnFrom int(10) value;
  columnTo int(10) value;
  width float(8) value;
  format pointer value;
end-pr;

dcl-pr worksheet_set_column_opt int(10) extproc('worksheet_set_column_opt');
  worksheet pointer value;
  columnFrom int(10) value;
  columnTo int(10) value;
  width float(8) value;
  format pointer value;
  options pointer value;
end-pr;

dcl-pr worksheet_merge_range int(10) extproc('worksheet_merge_range');
  worksheet pointer value;
  rowFrom int(10) value;
  columnFrom int(10) value;
  rowTo int(10) value;
  columnTo int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_freeze_panes extproc('worksheet_freeze_panes');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
end-pr;

dcl-pr worksheet_split_panes extproc('worksheet_split_panes');
  worksheet pointer value;
  vertical float(8) value;
  horizontal float(8) value;
end-pr;

dcl-pr worksheet_set_selection extproc('worksheet_set_selection');
  worksheet pointer value;
  rowFrom int(10) value;
  columnFrom int(10) value;
  rowTo int(10) value;
  columnTo int(10) value;
end-pr;

dcl-pr worksheet_set_landscape extproc('worksheet_set_landscape');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_set_portrait extproc('worksheet_set_portrait');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_set_page_view extproc('worksheet_set_page_view');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_hide_zero extproc('worksheet_hide_zero');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_write_number int(10) extproc('worksheet_write_number');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  number float(8) value;
  format pointer value;
end-pr;

dcl-pr worksheet_write_datetime int(10) extproc('worksheet_write_datetime');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  datetime likeds(lxw_datetime);
  format pointer value;
end-pr;

dcl-pr workbook_add_chart pointer extproc('workbook_add_chart');
  worksheet pointer value;
  charttype int(10) value;
end-pr;

dcl-pr worksheet_insert_chart extproc('worksheet_insert_chart');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  chart pointer value;
end-pr;

// Note : Not in current ILE C version included.
// dcl-pr workbook_add_chartsheet pointer extproc('workbook_add_chartsheet');
//   workbook pointer value;
//   worksheet pointer value options(*string);
// end-pr;

dcl-pr worksheet_repeat_rows int(10) extproc(*dclcase);
  worksheet pointer value;
  firstRow int(10) value;
  lastRow int(10) value;
end-pr;

dcl-pr worksheet_set_header int(10) extproc(*dclcase);
  worksheet pointer value;
  string pointer value options(*string);
end-pr;

dcl-pr worksheet_set_header_opt int(10) extproc(*dclcase);
  worksheet pointer value;
  string pointer value options(*string);
  options likeds(lxw_header_footer_options_t);
end-pr;

dcl-pr worksheet_set_footer int(10) extproc(*dclcase);
  worksheet pointer value;
  string pointer value options(*string);
end-pr;

dcl-pr worksheet_set_footer_opt int(10) extproc(*dclcase);
  worksheet pointer value;
  string pointer value options(*string);
  options likeds(lxw_header_footer_options_t);
end-pr;

dcl-pr worksheet_set_margins extproc(*dclcase);
  worksheet pointer value;
  left float(8) value;
  right float(8) value;
  top float(8) value;
  bottom float(8) value;
end-pr;

dcl-pr worksheet_print_row_col_headers extproc(*dclcase);
  worksheet pointer value;
end-pr;

dcl-pr format_set_font_name extproc('format_set_font_name');
  format pointer value;
  fontname pointer value options(*string);
end-pr;

dcl-pr format_set_font_size extproc('format_set_font_size');
  format pointer value;
  size float(8) value;
end-pr;

dcl-pr format_set_font_color extproc('format_set_font_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_bold extproc('format_set_bold');
  format pointer value;
end-pr;

dcl-pr format_set_italic extproc('format_set_italic');
  format pointer value;
end-pr;

dcl-pr format_set_underline extproc('format_set_underline');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_font_strikeout extproc('format_set_font_strikeout');
  format pointer value;
end-pr;

dcl-pr format_set_font_script extproc('format_set_font_script');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_num_format extproc('format_set_num_format');
  format pointer value;
  numberFormat pointer value options(*string);
end-pr;

dcl-pr format_set_align extproc('format_set_align');
  format pointer value;
  alignment uns(3) value;
end-pr;

dcl-pr format_set_pattern extproc('format_set_pattern');
  format pointer value;
  patternIndex uns(3) value;
end-pr;

dcl-pr format_set_bg_color extproc('format_set_bg_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_fg_color extproc('format_set_fg_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_border extproc('format_set_border');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_top extproc('format_set_top');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_bottom extproc('format_set_bottom');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_left extproc('format_set_left');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_right extproc('format_set_right');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_text_wrap extproc('format_set_text_wrap');
  format pointer value;
end-pr;

dcl-pr chart_add_series pointer extproc('chart_add_series');
  chart pointer value;
  categories pointer value options(*string);
  values pointer value options(*string);
end-pr;

dcl-pr chart_series_set_categories extproc('chart_series_set_categories');
  series pointer value;
  worksheet pointer value;
  firstrow int(10) value;
  firstcolumn int(10) value;
  lastrow int(10) value;
  lastcolumn int(10) value;
end-pr;

dcl-pr chart_series_set_values extproc('chart_series_set_values');
  series pointer value;
  worksheet pointer value;
  firstrow int(10) value;
  firstcolumn int(10) value;
  lastrow int(10) value;
  lastcolumn int(10) value;
end-pr;

dcl-pr chart_series_set_name extproc('chart_series_set_name');
  series pointer value;
  name pointer value options(*string);
end-pr;
 
dcl-pr chart_axis_get pointer extproc('chart_axis_get');
  chart pointer value;
  axis int(10) value;
end-pr;
 
dcl-pr chart_axis_set_name extproc('chart_axis_set_name');
  axis pointer value;
  name pointer value options(*string);
end-pr;
 
dcl-pr chart_series_set_labels extproc('chart_series_set_labels');
  series pointer value;
end-pr;
 
dcl-pr chart_axis_set_max extproc('chart_axis_set_max');
  axis pointer value;
  max float(8) value;
end-pr;
 
dcl-pr chart_set_table extproc('chart_set_table');
  chart pointer value;
end-pr;
 
dcl-pr chart_title_set_name extproc('chart_title_set_name');
  chart pointer value;
  title pointer value options(*string);
end-pr;
 

//
// 3rd Party Prototypes
//
dcl-pr lxw_datetime_create likeds(lxw_datetime) extproc(*dclcase);
  date date const options(*omit);
  time time const options(*nopass);
end-pr;


/endif
