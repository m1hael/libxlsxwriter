**FREE

ctl-opt nomain;

/include '../include/xlsxwriter_h.rpgle'


///
// Create datetime data structure
//
// Creates a datetime data structure which can be used to write dates and times
// into a cell with a proper format.
//
// A date or/and a time value can be passed to either get a date value, a time
// value or a timestamp value returned.
//
// @param date
// @param time
// @return datetime data structure
///
dcl-proc lxw_datetime_create export;
  dcl-pi *n likeds(lxw_datetime);
    date date const options(*omit);
    time time const options(*nopass);
  end-pi;
  
  dcl-ds datetime likeds(lxw_datetime) inz(*likeds);

  if (%addr(date) <> *null);
    datetime.year = %subdt(date : *YEARS);
    datetime.month = %subdt(date : *MONTHS);
    datetime.day = %subdt(date : *DAYS);
  endif;
  
  if (%parms() = 2);
    datetime.hour = %subdt(time : *HOURS);
    datetime.min = %subdt(time : *MINUTES);
    datetime.sec = %subdt(time : *SECONDS);
  endif;

  return datetime;
end-proc;
